import { NgModule } from '@angular/core';
import { NavigationStart, Router, RouterModule, Routes } from '@angular/router';
import { SessionService } from './service/session.service';
import { MyPlayersAdminComponent } from './ui/admin/my-players/my-players-admin.component';
import { HomeComponent } from './ui/home/home.component';
import { HomeClientComponent } from './ui/client/home/home.component'; 
import { LoginComponent } from './ui/login/login.component';
import { MyTeamsAdminComponent } from './ui/admin/my-teams/my-teams.component';

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { 
        path: 'admin', component: HomeComponent, children: [
            { path: 'home', component: MyPlayersAdminComponent },
            { path: 'my-teams', component: MyTeamsAdminComponent },
            { path: '', pathMatch: 'full', redirectTo: 'home' }
        ] 
    },
    {
        path: 'client', component: HomeComponent, children: [
            { path: 'home', component: HomeClientComponent },
            { path: '', pathMatch: 'full', redirectTo: 'home' }
        ]
    },
    { path: '', pathMatch: 'full', redirectTo: '/login' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

    constructor(private router: Router,
        private sessionService: SessionService) {
        this.setupRouterListener()
    }

    private setupRouterListener() {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (!this.sessionService.isLoggedIn && event.url !== "/login") {
                    this.router.navigateByUrl('/login')
                } else if (this.sessionService.isLoggedIn) {

                    if ((!this.sessionService.isAdmin && event.url.startsWith('/admin')) ||
                        (!this.sessionService.isClient && event.url.startsWith('/client')) ||
                        event.url === '/login' || event.url === '/') {
                        this.goToHome()
                    }
                }
            }
        })
    }

    private goToHome() {
        if (this.sessionService.isAdmin) {
            this.router.navigateByUrl('/admin')
        }

        return this.router.navigateByUrl('/client')
    }

}
