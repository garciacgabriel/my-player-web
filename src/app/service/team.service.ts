import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Team } from "../model/team.model";
import { WS_TEAM } from "./endpoints";

@Injectable(
    { providedIn: 'root' }
)
export class TeamService {

    constructor(private http: HttpClient) {

    }

    findAll() : Observable<Team[]> {
        return this.http.get<Team[]>(WS_TEAM)
    }

}