const URL_BASE = "http://localhost:8080"

export const WS_LOGIN = URL_BASE + "/login"
export const WS_PLAYER = URL_BASE + "/players"
export const WS_MY_PLAYER = URL_BASE + "/players/my-players"
export const WS_TEAM = URL_BASE + "/teams"