import { Component, Input, OnInit } from '@angular/core';
import { Team } from 'src/app/model/team.model';

@Component({
  selector: 'app-list-teams',
  templateUrl: './list-teams.component.html',
  styleUrls: ['./list-teams.component.css']
})
export class ListTeamsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name'];
  @Input() teams: Array<Team> = new Array<Team>();

  constructor() { }

  ngOnInit(): void {
  }

}
