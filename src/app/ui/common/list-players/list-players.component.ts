import { Component, Input, OnInit } from '@angular/core';
import { Player } from 'src/app/model/player.model';

@Component({
  selector: 'app-list-players',
  templateUrl: './list-players.component.html',
  styleUrls: ['./list-players.component.css']
})
export class ListPlayersComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'nationality', 'price'];
  @Input() players: Array<Player> = new Array<Player>();

  constructor() { }

  ngOnInit(): void {
  }

}
