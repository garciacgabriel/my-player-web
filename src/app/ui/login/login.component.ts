import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthData } from 'src/app/model/login.model';
import { SessionService } from 'src/app/service/session.service';
import { UserService } from 'src/app/service/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {

    formLogin = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
    })

    constructor(private formBuilder: FormBuilder,
                private userService: UserService,
                private sessionService: SessionService,
                private router: Router) { 
    }

    login() {
        let authData: AuthData = {
            login: this.username.value,
            password: this.password.value
        }

        this.userService.login(authData).subscribe(result => {
            this.sessionService.authenticate(result)
            this.router.navigateByUrl(this.sessionService.isAdmin ? "/admin" : "/client")
        }, 
        error => {
            alert('Dados inválidos para o login!')
        })
    }

    get username() {
        return this.formLogin.controls.username
    }

    get password() {
        return this.formLogin.controls.password
    }

}
